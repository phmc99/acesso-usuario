import logo from "./logo.svg";
import "./App.css";

import { useState } from "react";
import { RestrictedPage } from "./components/RestrictedPage";

function App() {
  const [isLoggedIn, setLoggedIn] = useState(false);

  const name = "Pedro";

  const logIn = () => {
    return isLoggedIn ? console.log("Logado") : setLoggedIn(true);
  };
  const logOut = () => {
    return isLoggedIn ? setLoggedIn(false) : console.log("Deslogado");
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <RestrictedPage
          isLoggedIn={isLoggedIn}
          user={name}
          Login={logIn}
          Logout={logOut}
        />
      </header>
    </div>
  );
}

export default App;
