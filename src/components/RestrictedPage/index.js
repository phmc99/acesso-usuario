import "./style.css";

export function RestrictedPage({ isLoggedIn, user, Login, Logout }) {
  return (
    <div>
      {isLoggedIn ? (
        <div className="estilo">
          <span>Bem vindo, {user}!</span>
          <button onClick={Logout}>Logout</button>
        </div>
      ) : (
        <div className="estilo">
          <span>Acesso negado</span>
          <button onClick={Login}>Login</button>
        </div>
      )}
    </div>
  );
}
